# README #

Running with jar:

java -jar EventNet.jar p1 p2

p1:

+ ls3-t: Evnet network for temporal relation with LS3RyIE

+ ie5-t: Evnet network for temporal relation with Open IE 5

+ ls3-c1: Evnet network for causal relation with LS3RyIE (Causal-TimeBank)

+ ls3-c2: Evnet network for causal relation with LS3RyIE (Causal-TimeBank-Ex)

+ ie5-c1: Evnet network for causal relation with Open IE 5 (Causal-TimeBank)

+ ie5-c2: Evnet network for causal relation with Open IE 5 (Causal-TimeBank-Ex)

p2:

+ name of document

Example: java -jar EventNet.jar ls3-t wsj_0026.col


