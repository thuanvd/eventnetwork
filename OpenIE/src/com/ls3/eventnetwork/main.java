package com.ls3.eventnetwork;

import java.io.IOException;

import com.ls3.oie.temporalrules.mainProcessCausalE;
import com.ls3.oie.temporalrules.mainProcessCausalIE5;
import com.ls3.oie.temporalrules.mainProcessTempIE;
import com.ls3.oie.temporalrules.mainProcessTempIE5;

public class main {
	
	public static void main(String args[]) throws IOException{
		
		if (args.length==2) {
			// temporal events
			if (args[0].equals("ls3-t")) {
				System.out.println(args[0]);
				System.out.println(args[1]);
				mainProcessTempIE.startTempIE(args[1]);
			}else if (args[0].equals("ie5-t")) {
				mainProcessTempIE5.startTempIE5(args[1]);
			}
			// causal events LS3
			else if (args[0].equals("ls3-c1")) {
				mainProcessCausalE.startCausalIE1(args[1]);
			}else if (args[0].equals("ls3-c2")) {
				mainProcessCausalE.startCausalIE2(args[1]);;
			}else if (args[0].equals("ie5-c1")) {
				mainProcessCausalIE5.startCausalIE1(args[1]);
			}else if (args[0].equals("ie5-c2")) {
				mainProcessCausalIE5.startCausalIE2(args[1]);
			}			
		}else{
			System.out.println("Invalid inputs...");
			
		}
			
	}	
}
